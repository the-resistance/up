import fs from 'fs'
import { packToFs } from 'ipfs-car/pack/fs'
import { CarIndexedReader } from '@ipld/car'
import { NFTStorage } from 'nft.storage'

const endpoint = 'https://api.nft.storage' // the default
const token = process.env.TOKEN // your API key from https://nft.storage/manage
const item = process.env.ITEM

async function main() {
  const storage = new NFTStorage({ endpoint, token })

  // locally chunk'n'hash the file to get the CID and pack the blocks in to a CAR
  const { root } = await packToFs({
    input: `${process.cwd()}/${item}`,
    output: `${process.cwd()}/${item}.car`,
  })

  const expectedCid = root.toString()
  console.log({ expectedCid })

  // Create the car reader
  const carReader = await CarIndexedReader.fromFile(
    `${process.cwd()}/${item}.car`
  )

  console.log('Uploading...')

  // send the CAR to nft.storage, the returned CID will match the one we created above.
  const cid = await storage.storeCar(carReader)

  // verify the service stored the CID we expected
  const cidsMatch = expectedCid === cid
  console.log({ cid, expectedCid, cidsMatch })

  // check that the CID is pinned
  const status = await storage.status(cid)
  console.log(status)

  // Delete car file created
  await fs.promises.rm(`${process.cwd()}/${item}.car`)
}
main()
