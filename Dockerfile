FROM node:18-buster-slim

WORKDIR /app

COPY package*.json up.js ./

RUN npm install --production

CMD ["npm", "start"]